-- MySQL dump 10.16  Distrib 10.1.44-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: numismatics
-- ------------------------------------------------------
-- Server version	10.1.44-MariaDB-0+deb9u1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Coin`
--

DROP TABLE IF EXISTS `Coin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Coin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `value` float DEFAULT NULL,
  `state` int(11) DEFAULT NULL,
  `denomination` varchar(255) DEFAULT NULL,
  `date_coined` int(11) NOT NULL,
  `amount` int(11) DEFAULT NULL,
  `material` varchar(255) DEFAULT NULL,
  `portrait` int(11) DEFAULT NULL,
  `horn_size` bit(1) DEFAULT b'0',
  `standard` bit(1) DEFAULT b'1',
  `grade` varchar(10) DEFAULT 'M',
  `rarity` varchar(10) DEFAULT 'CC',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Coin`
--

LOCK TABLES `Coin` WRITE;
/*!40000 ALTER TABLE `Coin` DISABLE KEYS */;
INSERT INTO `Coin` VALUES (1,1,1,'',1234,1,NULL,NULL,NULL,NULL,'M','CC'),(2,1,1,'Lira',1946,1,'',NULL,'\0','\0','M','CC'),(3,1,1,'Lira',1946,1,'',NULL,'\0','\0','M','CC'),(4,1,1,'Lira',1946,2,'Italma',NULL,'\0','\0','M','CC'),(5,1,1,'Lira',1946,2,'Italma',NULL,'\0','\0','M','CC'),(6,1,1,'Lira',1946,1,NULL,NULL,'\0','\0','M','CC'),(7,2,1,'Lira',1948,3,'italma',NULL,'','','M','CC'),(8,5,1,'Lira',1951,1,'Italma',1,'\0','','M','CC'),(9,20,1,'Lira',1957,1,NULL,1,'\0','','M','CC'),(10,0.05,2,'Lira',1941,1,'Rame',2,'\0','','M','CC'),(11,200,1,'Lira',1988,1,'Rame',1,'\0','','M','CC'),(12,0.05,3,'Franco',1944,2,NULL,3,'','','M','CC'),(13,0.2,2,'Lira',1941,1,'Argento',2,'\0','','M','CC');
/*!40000 ALTER TABLE `Coin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Portraited`
--

DROP TABLE IF EXISTS `Portraited`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Portraited` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `surname` varchar(255) DEFAULT NULL,
  `sovreign_name` varchar(255) DEFAULT NULL,
  `birth` datetime DEFAULT NULL,
  `death` datetime DEFAULT NULL,
  `state` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Portraited`
--

LOCK TABLES `Portraited` WRITE;
/*!40000 ALTER TABLE `Portraited` DISABLE KEYS */;
INSERT INTO `Portraited` VALUES (1,'Ragazza ',NULL,NULL,NULL,NULL,1),(2,'Vittorio Emanuele III',NULL,NULL,NULL,NULL,2),(3,'Donna',NULL,NULL,NULL,NULL,3);
/*!40000 ALTER TABLE `Portraited` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `State`
--

DROP TABLE IF EXISTS `State`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `State` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name_int` varchar(255) DEFAULT NULL,
  `foundation` int(11) DEFAULT NULL,
  `expiration` int(11) DEFAULT NULL,
  `actual_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `State`
--

LOCK TABLES `State` WRITE;
/*!40000 ALTER TABLE `State` DISABLE KEYS */;
INSERT INTO `State` VALUES (1,NULL,NULL,NULL,'Repubblica italiana'),(2,NULL,NULL,NULL,'Regno d\'Italia'),(3,NULL,NULL,NULL,'Svizzera');
/*!40000 ALTER TABLE `State` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-04-02 19:40:15
