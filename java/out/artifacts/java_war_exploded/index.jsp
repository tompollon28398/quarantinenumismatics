<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>--%>
<%--
  Created by IntelliJ IDEA.
  User: tompollon
  Date: 3/29/20
  Time: 10:50 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<body>
<head>
    <title>Numismatica</title>
    <link rel="shortcut icon" type="image/png" href="${pageContext.request.contextPath}/img/50lire.png"/>
    <link rel="stylesheet" type="text/css" media="all" href="css/index.css" th:href="@{index.css}" />
</head>

<main>
<h1>Numismatica in quatantena</h1>
Cerca le tue monete preferite o visualizza un elenco di quelle cha abbiamo nel nostro database
<h2>Insert a new coin here</h2>
<a   href="insertionJSP.jsp"> Click here</a>
<h3>See all the coins here:</h3>
<a   href="ShowCoins?value=1"> Show me the archive!</a>
</main>

<footer>
    <p align="center">Powered by: Tommaso Polloni    <a href="mailto:tommaso.polloni@protonmail.ch">
        tommaso.polloni@protonmail.ch</a>
    </p>
</footer>

</body>
</html>
