package it.polloni.numismatica.beans;

public class Bean {
    private final Coin coin;
    private final State state;
    private final Portraited portraited;

    public Bean(Coin coin, State state, Portraited portraited) {
        this.coin = coin;
        this.state = state;
        this.portraited = portraited;
    }

    public Coin getCoin() {
        return coin;
    }

    public State getState() {
        return state;
    }

    public Portraited getPortraited() {
        return portraited;
    }
}
