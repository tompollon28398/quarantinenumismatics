package it.polloni.numismatica.beans;

public class State {
    private int id;
    private String name_int;
    private int foundation;
    private int expiration;
    private String actual_name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName_int() {
        return name_int;
    }

    public void setName_int(String name_it) {
        this.name_int = name_it;
    }

    public int getFoundation() {
        return foundation;
    }

    public void setFoundation(int foundation) {
        this.foundation = foundation;
    }

    public int getExpiration() {
        return expiration;
    }

    public void setExpiration(int expiration) {
        this.expiration = expiration;
    }

    public String getActual_name() {
        return actual_name;
    }

    public void setActual_name(String actual_name) {
        this.actual_name = actual_name;
    }
}
