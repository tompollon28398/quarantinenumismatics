package it.polloni.numismatica.beans;

import java.math.BigDecimal;

public class Coin {
    //TODO: capire come gestire la situazione dello stato
    private Integer id=null;
    private Float value=null;
    private Integer state=null;
    private String denomination=null;
    private Integer date_coined=null;
    private Integer amount=1;
    private String material=null;
    private Integer portrait=null;
    private Boolean horn_size=null;
    private Boolean standard=null;
    private String grade;
    private String rarity=null;
    private BigDecimal price=null;


    public Coin() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public float getValue() {
        return value;
    }

    public void setValue(float value) {
        this.value = value;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public String getRarity() {
        return rarity;
    }

    public void setRarity(String rarity) {
        this.rarity = rarity;
    }

    public String getDenomination() {
        return denomination;
    }

    public void setDenomination(String denomination) {
        this.denomination = denomination;
    }

    public int getDate_coined() {
        return date_coined;
    }

    public void setDate_coined(int date_coined) {
        this.date_coined = date_coined;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getMaterial() {
        return material;
    }

    public void setMaterial(String material) {
        this.material = material;
    }

    public Integer getPortrait() {
        return portrait;
    }

    public void setPortrait(Integer portrait) {
        this.portrait = portrait;
    }

    public Boolean isHorn_size() {
        return horn_size;
    }

    public void setHorn_size(Boolean horn_size) {
        this.horn_size = horn_size;
    }

    public Boolean isStandard() {
        return standard;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public void setStandard(boolean standard) {
        this.standard = standard;
    }
}
