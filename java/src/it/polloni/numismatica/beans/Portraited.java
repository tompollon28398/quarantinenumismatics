package it.polloni.numismatica.beans;

import java.sql.Timestamp;

public class Portraited {
    private int id;
    private String name;
    private String surname;
    private String sovreign_name;
    private java.sql.Timestamp birth;
    private java.sql.Timestamp death;
    private State state;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getSovreign_name() {
        return sovreign_name;
    }

    public void setSovreign_name(String sovreign_name) {
        this.sovreign_name = sovreign_name;
    }

    public Timestamp getBirth() {
        return birth;
    }

    public void setBirth(Timestamp birth) {
        this.birth = birth;
    }

    public Timestamp getDeath() {
        return death;
    }

    public void setDeath(Timestamp death) {
        this.death = death;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }
}
