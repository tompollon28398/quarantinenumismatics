package it.polloni.numismatica.controllers;

import it.polloni.numismatica.beans.Bean;
import it.polloni.numismatica.beans.Coin;
import it.polloni.numismatica.beans.Portraited;
import it.polloni.numismatica.beans.State;
import it.polloni.numismatica.dao.PortraitDAO;
import it.polloni.numismatica.dao.StateDAO;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.UnavailableException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sound.sampled.Port;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@WebServlet(name = "ShowCoins")
public class ShowCoins extends HttpServlet {
    private Connection c =null;
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    public void init() throws ServletException{
        try {
            ServletContext context = getServletContext();
            String driver = context.getInitParameter("dbDriver");
            String url = context.getInitParameter("dbUrl");
            String user = context.getInitParameter("dbUser");
            String password = context.getInitParameter("dbPassword");
            Class.forName(driver);
            c = DriverManager.getConnection(url, user, password);

        } catch (ClassNotFoundException e) {
            throw new UnavailableException("Can't load database driver");
        } catch (SQLException e) {
            throw new UnavailableException("Couldn't get db connection");
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String error = "";
        String query = "SELECT id, value, state, denomination, date_coined, amount, material, portrait, horn_size, standard FROM Coin";
        ResultSet result = null;
        PreparedStatement pstatement = null;
        List<Bean> beans = new ArrayList<Bean>();
        try {
            pstatement = c.prepareStatement(query);
            result = pstatement.executeQuery();
            while (result.next()) {
                Coin coin = new Coin();
                coin.setId(result.getInt("id"));
                coin.setValue(result.getFloat("value"));
                coin.setState(result.getInt("state"));
                coin.setDenomination(result.getString("denomination"));
                coin.setDate_coined(result.getInt("date_coined"));
                coin.setAmount(result.getInt("amount"));
                coin.setMaterial(result.getString("material"));
                coin.setPortrait(result.getInt("portrait"));
                coin.setHorn_size(result.getBoolean("horn_size"));
                coin.setStandard(result.getBoolean("standard"));
                StateDAO sDAO = new StateDAO(c);
                State state =sDAO.getStateByID(coin.getState());
                //TODO
                Portraited p = new Portraited();
                Bean bean = new Bean(coin, state, p);
                beans.add(bean);
            }
        } catch (SQLException e) {
            error = "SQL QUERY EXECUTION ERROR";
        } finally {
            try {
                result.close();
            } catch (Exception e1) {
                error = "SQL RESULTSET ERROR";
            }
            try {
                pstatement.close();
            } catch (Exception e1) {
                error = "SQL STATEMENT ERROR";
            }
            if (error == "") {
                String path = "/ShowCoins.jsp";
                request.setAttribute("number", beans.size());
                request.setAttribute("beans", beans);
                RequestDispatcher dispatcher = request.getRequestDispatcher(path);
                dispatcher.forward(request, response);
            } else {
                response.sendError(500, error);
            }
        }
    }
}
