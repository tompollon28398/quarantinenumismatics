package it.polloni.numismatica.controllers;

import it.polloni.numismatica.beans.Coin;
import it.polloni.numismatica.dao.CoinDAO;
import it.polloni.numismatica.dao.PortraitDAO;
import it.polloni.numismatica.dao.StateDAO;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.UnavailableException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

@WebServlet(name = "InsertCoin")
public class InsertCoin extends HttpServlet {
    private Connection connection;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Coin coin = new Coin();
        String error = "";
        coin.setValue(Float.parseFloat(request.getParameter("value")));
        String stateName = request.getParameter("state");
        try {
            StateDAO stateDAO = new StateDAO(connection);
            coin.setState(stateDAO.returnState(stateName));
        } catch (SQLException e) {
            error += "An error occured while retrieving the state from the database "+ e.getMessage();
            e.printStackTrace();
        }
        coin.setDenomination(request.getParameter("description"));
        coin.setDate_coined(Integer.parseInt(request.getParameter("date")));
        if(!request.getParameter("amount").isEmpty())
            coin.setAmount(Integer.parseInt(request.getParameter("amount")));
        if(!request.getParameter("material").isEmpty())
            coin.setMaterial(request.getParameter("material"));
        String portrait = request.getParameter("portrait");
        if(!request.getParameter("material").isEmpty())
            coin.setMaterial(request.getParameter("material"));
        try {
            PortraitDAO portraitDAO = new PortraitDAO(connection);
            coin.setPortrait(portraitDAO.returnPortraitID(portrait, coin.getState()));
        } catch (SQLException e) {
            error += "An error occured while retrieving the portrait from the database "+ e.getMessage();
            e.printStackTrace();
        }
        if(request.getParameterValues("horn_size")!= null){
            coin.setHorn_size(true);
        }else
            coin.setHorn_size(false);
        if(request.getParameterValues("standard")!=null){
            coin.setStandard(true);
        }else
            coin.setStandard(false);
        if(request.getParameter("rarity")!= null)
            coin.setRarity(request.getParameter("rarity"));
        if(request.getParameter("grade")!= null)
            coin.setRarity(request.getParameter("grade"));
        if(request.getParameter("price")!= null)
            coin.setPrice(new BigDecimal(request.getParameter("price")));

        CoinDAO coinDAO = new CoinDAO(connection);
        try{
            coinDAO.insertCoin(coin);
        }catch (SQLException e){
              error += " Impossible to insert selected coin into database " + e.getMessage();
              e.printStackTrace();
        }

        if (error != "") {
            response.sendError(505, error);
        }
        else{
            String path = getServletContext().getContextPath() +  "/ShowCoins";
                response.sendRedirect(path);
        }
    }

    public void init() throws ServletException{
        //TODO: come evitare di ripetere sempre queste linee di codice?
        try{
            ServletContext context = getServletContext();
            String driver = context.getInitParameter("dbDriver");
            String url = context.getInitParameter("dbUrl");
            String user = context.getInitParameter("dbUser");
            String password = context.getInitParameter("dbPassword");
            Class.forName(driver);
            connection = DriverManager.getConnection(url, user, password);
        } catch (ClassNotFoundException e) {
            throw new UnavailableException("Unable to find the database driver");
        } catch (SQLException e) {
            throw new javax.servlet.UnavailableException("Unable to get the DB connection");
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
