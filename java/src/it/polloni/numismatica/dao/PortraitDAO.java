package it.polloni.numismatica.dao;

import it.polloni.numismatica.beans.State;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class PortraitDAO {
    Connection c;
    private String FIND_PORTRAIT = "SELECT id FROM Portraited " +
            "WHERE name = ? OR sovreign_name = ?";
    private String CREATE_PORTRAITED = "INSERT INTO Portraited(name,state) VALUES (?, ?)";
    private String LAST_INSERT = "SELECT LAST_INSERT_ID()";
    public PortraitDAO(Connection c) {
        this.c = c;
    }

    public int returnPortraitID(String name, Integer stateID) throws SQLException {
        //if the selected state is in the database, return the ID, otherwise create it and do the same
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = c.prepareStatement(FIND_PORTRAIT);
            ps.setString(1, name);
            ps.setString(2, name);
            rs = ps.executeQuery();
            if (rs.next()) {
                return rs.getInt("id");
            } else {
                PreparedStatement ps2 = c.prepareStatement(CREATE_PORTRAITED);
                ps2.setString(1, name);
                ps2.setInt(2, stateID);
                ps2.executeUpdate();
                PreparedStatement ps3 = c.prepareStatement(LAST_INSERT);
                rs = ps3.executeQuery();
                if (rs.next()) {
                    return rs.getInt("LAST_INSERT_ID()");
                } else
                    throw new SQLException("Impossibe to add portaited to database");
            }
        } catch (SQLException e) {
            throw new SQLException();
        } finally {
            try {
                ps.close();
                rs.close();
            } catch (Exception e1) {
            }
        }
    }

    /*public State getStateByID(Integer id) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = c.prepareStatement(FIND_BY_ID);
            ps.setInt(1, id);
            rs = ps.executeQuery();
            if (rs.next()) {
                State to_ret = new State();
                to_ret.setActual_name(rs.getString("actual_name"));
                to_ret.setFoundation(rs.getInt("foundation"));
                to_ret.setExpiration(rs.getInt("expiration"));
                to_ret.setName_it(rs.getString("name_int"));
                return to_ret;
            } else
                return null;
        } catch (SQLException e) {
            throw new SQLException("Something went wrong during stateID retrieval");
        } finally {
            try {
                ps.close();
                rs.close();
            } catch (Exception e) {

            }
        }
    }*/

}
