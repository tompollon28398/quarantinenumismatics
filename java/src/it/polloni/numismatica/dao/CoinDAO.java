package it.polloni.numismatica.dao;

import it.polloni.numismatica.beans.Coin;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Types;

public class CoinDAO {
    private Connection c;
    private String INSERT_QUERY =
            "INSERT INTO numismatics.Coin (value, state, denomination," +
                    "date_coined, amount, material, portrait," +
                    "horn_size, standard, price) VALUES(" +
                    "?,?,?,?,?,?,?,?,?,?)";



    public CoinDAO(Connection c){ this.c = c;}

    public void insertCoin(Coin coin) throws SQLException {
        PreparedStatement ps=null;
        try{
            ps = c.prepareStatement(INSERT_QUERY);
            ps.setFloat(1, coin.getValue());
            ps.setInt(2, coin.getState());
            if(coin.getDenomination()!= null)
                ps.setString(3, coin.getDenomination());
            else
                ps.setNull(3, Types.VARCHAR);
            ps.setInt(4, coin.getDate_coined());
            ps.setInt(5, coin.getAmount());
            if(coin.getMaterial() != null) {
                ps.setString(6, coin.getMaterial());
            }else
                ps.setNull(6, Types.VARCHAR);
            if(coin.getPortrait() != null) {
                ps.setInt(7, coin.getPortrait());
            }else
                ps.setNull(7, Types.INTEGER);
            if(coin.isHorn_size()!= null) {
                ps.setBoolean(8, coin.isHorn_size());
            }else
                ps.setNull(8, Types.BOOLEAN);
            if(coin.isStandard()!= null) {
                ps.setBoolean(9, coin.isStandard());
            }else
                ps.setNull(9, Types.BOOLEAN);
            if(coin.getPrice()!= null)
                ps.setBigDecimal(10, coin.getPrice());
            else
                ps.setNull(10, Types.DECIMAL);

            try{
                ps.executeUpdate();
            }catch(SQLException e){
                throw new SQLException("Unable to complete update. Error in SQL syntax?", e);
            }
        }catch(SQLException e){
            throw new SQLException("Unable to prepare statement: connection problems?", e);
        }finally {
            try{
                ps.close();
            }catch (Exception e1){
                //we do not throw this exception?
            }
        }
    }
}
