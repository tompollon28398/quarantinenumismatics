<%@ page
        import="java.io.*,java.util.*, java.util.Iterator, it.polloni.numismatica.beans.*" %>
<%@ page import="it.polloni.numismatica.dao.StateDAO" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="java.sql.DriverManager" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1" %>
<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>All the coins we have</title>
    <link rel="shortcut icon" type="image/png" href="${pageContext.request.contextPath}/img/50lire.png"/>
    <link rel="stylesheet" type="text/css" media="all" href="css/showCoins.css" th:href="@{showCoins.css}"/>
</head>

<body>
<p align="center">
    Total number of coins:
    <%=request.getAttribute("number")%>
</p>
<table border="1" align="center">
    <tr>
        <td>value</td>
        <td>state</td>
        <td>Denomination</td>
        <td>Year</td>
        <td>Amount</td>
    </tr>
    <%
        List<Bean> coins = (List<Bean>) request.getAttribute("beans");
        Iterator<Bean> iterator = coins.iterator();
        while (iterator.hasNext()) {
            Bean bean = (Bean) iterator.next();
            float value = bean.getCoin().getValue();
            String denomination = bean.getCoin().getDenomination();
            Integer year = bean.getCoin().getDate_coined();
            String stateName = bean.getState().getActual_name();
            Integer amount = bean.getCoin().getAmount();
            out.print("<tr><td>" + value + "</td>");
            out.println("<td>" + stateName + "</td>");
            out.println("<td>" + denomination + "</td>");
            out.println("<td>" + year + "</td>");
            out.println("<td>" + amount + "</td></tr>");
        }
    %>
</table>
</body>

<table align="center">
    <tr>
        <td><a href="index.jsp"> Home</a></td>
        <td><a href="insertionJSP.jsp"> Insert another</a></td>
    </tr>

</table>




</html>