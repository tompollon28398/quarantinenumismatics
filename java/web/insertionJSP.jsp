<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: tompollon
  Date: 3/29/20
  Time: 2:53 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Add a coin!</title>
    <link rel="shortcut icon" type="image/png" href="${pageContext.request.contextPath}/img/50lire.png"/>
    <link rel="stylesheet" type="text/css" media="all" href="css/insert.css" th:href="@{insert.css}"/>
</head>

<div class="row">
    <div class="column">

        <a name=conservazioni><h3>Grado di conservazione:</h3></a>
        sigla con cui si identifica il grado di usura di una moneta:
        <ul>
            <li><b>FDC</b>: fior di conio, moneta che non presenta segni di circolazione
            <li><b>SPL</b>: splendido, moneta che ha circolato pochissimo e in cui tutti i rilievi sono integri
            <li><b>BB</b>: bellissimo, moneta che ha circolato e in cui i rilievi maggiori presentano segni di
                usura,
                cionostante la moneta risulta perfettamente leggibile
            <li><b>MB</b>: molto bello, moneta usurata in cui alcune parti non sono leggibili
            <li><b>B</b>: bello, moneta liscia in cui la maggioranza dei rilievi sono scomparsi
            <li><b>D</b>: discreto, moneta quasi completamente liscia in cui i rilievi appena si intuiscono o con
                forti segni
                deturpanti
        </ul>
    </div>
    <div class="column">
        <h2 align="center">Create a new coin</h2>
        <table border="1" align="center">
            <c:url value="/InsertCoin" var="coinUrl"> </c:url>
            <form method="post" action="${coinUrl}">
                <tr>
                    <td> Value:</td>
                    <td><input name="value" type="number" step="0.01" required value="1"></td>
                </tr>
                <tr>
                    <td> Currency unit:</td>
                    <td><input name="description" type="text" required value="Lira"></td>
                </tr>
                <tr>
                    <td> Date:</td>
                    <td><input name="date" type="number" value="1946" required></td>
                </tr>
                <tr>
                    <td> State:</td>
                    <td><input name="state" type="text" value="Repubblica italiana" required></td>
                </tr>
                <tr>
                    <td> Amount:</td>
                    <td><input name="amount" type="number" value="1" required></td>
                </tr>
                <tr>
                    <td> Material:</td>
                    <td><input name="material" type="text"></td>
                </tr>
                <tr>
                    <td> Portrait:</td>
                    <td><input name="portrait" type="text"></td>
                </tr>
                <tr>
                    <td> Horn size:</td>
                    <td><input name="horn_size" type="checkbox"></td>
                </tr>
                <tr>
                    <td> Standard:</td>
                    <td><input name="standard" type="checkbox" value="true"></td>
                </tr>
                <tr>
                    <td> Rarity:</td>
                    <td><select name="rarity">
                        <option value="C">C: comune</option>
                        <option value="NC">NC: non comune</option>
                        <option value="R">R: rara</option>
                        <option value="R2">R2: molto rara</option>
                        <option value="R3">R3: rarissima</option>
                        <option value="R4">R4: estremamente rara</option>
                        <option value="R5">R5: solo alcuni esemplari</option>
                        <option value="U">U: unica</option>
                    </select></td>
                </tr>
                <tr>
                    <td> Grade:</td>
                    <td><select name="grade">
                        <option value="D">D: Discreto</option>
                        <option value="B">B: Bello</option>
                        <option value="MB">MB: Molto Bello</option>
                        <option value="BB">BB: Bellissimo</option>
                        <option value="SPL">SPL: Splendido</option>
                        <option value="FDC">FDC: Fior Di Conio</option>
                    </select></td>
                </tr>
                <tr>
                    <td>Price:</td>
                    <td><input name="price" type="number" style="display:table-cell; width:auto" min="0.00" max="10000.00" step="0.01"> €</td>
                </tr>
                <tr>
                    <td><a href="index.jsp"> Back to Home</a></td>
                    <td><input type="submit" value="Ok!"></td>
                </tr>

            </form>
            </td>
        </table>
    </div>
    <div class="column">
        <a name=rara><h3>Rarit&agrave;:</h3></a>
        indicazione della rarit&agrave; della moneta:
        <ul>
            <li><b>C</b>: comune
            <li><b>NC</b>: non comune
            <li><b>R</b>: rara
            <li><b>R<sup>2</sup></b>: molto rara
            <li><b>R<sup>3</sup></b>: rarissima
            <li><b>R<sup>4</sup></b>: estremamente rara
            <li><b>R<sup>5</sup></b>: solo alcuni esemplari
            <li><b>U</b>: unica
        </ul>
        Attenzione, il grado di rarit&agrave; riportato per ciascuna moneta &egrave; un&#39;indicazione mia
        personale che spesso e volentieri
        differisce da quelle che si possono trovare sui prezzari in commercio. Per i neofiti: la rarit&agrave; di
        una
        moneta incide sicuramente sul suo valore, per&ograve; questo non significa che una moneta rara sia anche
        preziosa;
        conta
        molto di pi&uacute; lo stato di conservazione.


    </div>
</div>
</div>


<%--<footer>
    <p align="center">Powered by: Tommaso Polloni</p>
    <p align="center">
        Mail me: <a href="mailto:tommaso.polloni@protonmail.ch">
        tommaso.polloni@protonmail.ch</a>
    </p>
</footer>--%>

</html>

